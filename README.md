# efinx_test

#โจทย์
ข้อที่ 1.   Function parameters: array of integer และ ค่าที่ต้องการจะค้นหา. 
Return: index ของ array ที่มีค่าเท่ากับค่าที่ต้องการจะค้นหา โดยในกรณีที่มีหลาย index ที่มีค่าเท่ากับค่าที่ต้องการจะค้นหา ต้องการให้โอกาสที่จะ return แต่ละ index ในกลุ่มคำตอบนั้น มีความน่าจะเป็นเท่าๆกัน

ข้อที่ 2. เขียน function สำหรับ decompress string ตามตัวอย่างต่อไปนี้
Input: 2[abc]3[ab]c
Output: abcabcabababc
Input: 10[a]c2[ab]
Output: aaaaaaaaaacabab
Input: 2[3[a]b]
Output: aaabaaab

ถ้าพลังเหลือ จะเขียนให้ข้อ 2 ให้ทำงานเป็น multi-thread เพื่อเพิ่มความเร็ว หรือ เขียนตัว compress แทนที่จะเป็น decompress มาดูก็ได้นะคะ 


