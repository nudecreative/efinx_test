
import java.lang.Math; // headers MUST be above the first class
import java.util.List;
import java.util.ArrayList;

public class FindIndexofArray {

    public static int findIndexofArray(int[] input, int target) {

        List<Integer> picked = new ArrayList<Integer>();

        do {
            int rand = (int) (Math.random() * input.length);

            if (picked.contains(rand)) {
                continue;
            } else {
                picked.add(rand);
                if (input[rand] == target) {
                    return rand;
                } else {
                    continue;
                }
            }
        } while (picked.size() < input.length);

        return -1;

    }

    public static void main(String[] args) {

        int[] q = { 10, 2, 3, 10, 20, 100, 10, 10, 55, 99, -5, 1000, 10 };

        int resultIs0 = 0, resultIs3 = 0, resultIs6 = 0, resultIs7 = 0, resultIs12 = 0;
        // test statistical data
        for (int i = 0; i < 100000; i++) {
            int result = findIndexofArray(q, 10);

            if (result == 0)
                resultIs0++;
            else if (result == 3)
                resultIs3++;
            else if (result == 6)
                resultIs6++;
            else if (result == 7)
                resultIs7++;
            else if (result == 12)
                resultIs12++;
            else 
                System.out.println("Unexpected result");
        }
        System.out.println("Prop. result of 0 is " + (double) resultIs0 / 100000);
        System.out.println("Prop. result of 3 is " + (double) resultIs3 / 100000);
        System.out.println("Prop. result of 6 is " + (double) resultIs6 / 100000);
        System.out.println("Prop. result of 7 is " + (double) resultIs7 / 100000);
        System.out.println("Prop. result of 12 is " + (double) resultIs12 / 100000);

    }
}