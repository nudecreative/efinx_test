import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Decompress {

    private static boolean isNumeric(char strNum) {
        return Character.isDigit(strNum);
    }

    private static int checkNumCharinParenthesis(String input) {

        int open = 0;
        int close = 0;
        for (int i = 0; i < input.length(); i++) {

            if (input.charAt(i) == '[') {
                open++;
            } else if (input.charAt(i) == ']') {
                close++;
            }

            if (open == close) {
                return i + 1;
            }
        }
        return input.length();
    }

    public static String decompress(String input) {

        String result = "";
        String numCache = "";

        for (int i = 0; i < input.length(); i++) {

            if (isNumeric(input.charAt(i))) {
                numCache += Character.toString(input.charAt(i));
            } else if (i == 0 && input.charAt(i) == '[') {
                // do nothing
            } else if (input.charAt(i) == '[') {

                // Find target of decompress text
                int howLongtoCompress = checkNumCharinParenthesis(input.substring(i, input.length()));
                String subResult = decompress(input.substring(i, i + howLongtoCompress));
                String subResultMultiplied = "";

                for (int subI = 0; subI < Integer.parseInt(numCache); subI++) {
                    subResultMultiplied += subResult;
                }

                result += subResultMultiplied;
                numCache = "";// Clear Multiplier
                // Skip i
                i += howLongtoCompress - 1;

            } else if (input.charAt(i) == ']') {

                return result;
            } else {
                result += input.charAt(i);
            }

        }

        return result;
    }

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService
                .submit(() -> System.out.println("Decompress 2[abc]3[ab]c result:" + decompress("2[abc]3[ab]c")));
        executorService.submit(() -> System.out.println("Decompress 10[a]c2[ab] result:" + decompress("10[a]c2[ab]")));
        executorService.submit(() -> System.out.println("Decompress 2[3[a]b] result:" + decompress("2[3[a]b]")));
        executorService.shutdown();

    }
}