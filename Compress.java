import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Compress {

    private static String onetimeCompress(String input) {

        String result = "";
        for (int i = 0; i < input.length(); i++) {

            boolean compressed = false;

            for (int lengthtoCompare = 1; lengthtoCompare <= (input.length() / 2); lengthtoCompare++) {
                int count = 0;
                if (i + lengthtoCompare > input.length())
                    break;
                String compareBaseline = input.substring(i, i + lengthtoCompare);

                for (int compareIndex = i + lengthtoCompare; compareIndex <= (input.length()
                        - lengthtoCompare); compareIndex += lengthtoCompare) {

                    if (input.substring(compareIndex, compareIndex + lengthtoCompare).equals(compareBaseline)) {
                        count++;
                    } else {

                        break;
                    }
                }

                if (count >= 1) {
                    i += ((count + 1) * lengthtoCompare) - 1;
                    result += String.valueOf(count + 1) + "[" + compareBaseline + "]";
                    compressed = true;
                    break;
                }

            }
            if (!compressed)
                result += Character.toString(input.charAt(i));
        }
        return result;
    }

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.submit(
                () -> System.out.println("Compress abcabcabababc result:" + (onetimeCompress("abcabcabababc"))));
        executorService.submit(
                () -> System.out.println("Compress aaaaaaaaaacabab result:" + (onetimeCompress("aaaaaaaaaacabab"))));
        executorService.submit(() -> System.out.println("Compress aaabaaab result:" + (onetimeCompress("aaabaaab"))));
        executorService.shutdown();

    }
}